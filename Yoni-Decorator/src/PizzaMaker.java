public class PizzaMaker {

    public static void main(String[] args) {
        Pizza basicPizza;
        basicPizza = new TomatoSauce(new Mozzarella(new Salami(new PlainPizza())));

        System.out.println("ingredients : " + basicPizza.getDescription());
        System.out.println("price : " + basicPizza.getCost());
    }
}
