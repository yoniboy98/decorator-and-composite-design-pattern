public class SongListGenerator {
    public static void main(String[] args) {
        SongComponent industrialMusic = new SongGroup("industrial" , " ");
        SongComponent heavyMetalMusic = new SongGroup("heavyMetalMusic" , " ");
        SongComponent dubstepMusic = new SongGroup("dubstepMusic " , " ");

        SongComponent everySong = new SongGroup("song list", "every song");
        everySong.add(industrialMusic);

        industrialMusic.add(new Song("head like a hole ", "NIN", 1990));
        industrialMusic.add(new Song("headhunter ", "front 242", 1988));

        industrialMusic.add(dubstepMusic);
        dubstepMusic.add(new Song("Centipde ","knife party ", 2012));
        dubstepMusic.add(new Song("Tetris ","Docter P ", 2011));

        dubstepMusic.add(heavyMetalMusic);

        heavyMetalMusic.add(new Song("war pigs ","black Sabath ",1970));
        heavyMetalMusic.add(new Song("ace of spades ","motorhead ",1980));

        DiscJocky crazyLarry = new DiscJocky(everySong);
        crazyLarry.getSongList();





    }
}
