public class Family extends FamilyTree {
        String firstName;
        String lastName;
        int releaseYear;
        String gender;

        Family(String getFirstName, String getLastName, int getYearOfBirth, String getGender){
        firstName = getFirstName;
        lastName = getLastName;
        releaseYear = getYearOfBirth;
        gender=getGender;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getYearOfBirth() {
        return releaseYear;
    }

    @Override
    public String getGender() {
        return gender;
    }


    public void displayInfo() {
        System.out.println(getFirstName()+getLastName()+ "has been a member of the family since "+ getYearOfBirth()+" and is a " + getGender()+"\n");
    }}
