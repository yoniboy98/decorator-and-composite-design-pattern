public class MainFamily {
    public static void main(String[] args) {
        FamilyTree me = new FamilyGroup("me = ","be like me");
        FamilyTree parents = new FamilyGroup("parents = ","take care of there kids");
        FamilyTree grandparents = new FamilyGroup("grandparents = ","cooking for the family");

        FamilyTree allFamilyMember = new FamilyGroup("all familymembers","working together for a better life");
        allFamilyMember.add(me);

        me.add(new Family("Yoni ","Vindelinckx ",1998," male"));

        me.add(parents);
        parents.add(new Family("Martine ","Meert ",1980," female"));
        parents.add(new Family("Serge ","Vindelinckx ",1970," male"));

        parents.add(grandparents);
        grandparents.add(new Family("Nicole"," Wouters ",1960," female"));
        grandparents.add(new Family(" Andre"," Meert ",1955," male"));

     Display displayFamily = new Display(allFamilyMember);
     displayFamily.getFamilyList();


    }
}
