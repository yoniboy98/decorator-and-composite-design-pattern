import java.util.ArrayList;


public class FamilyGroup extends FamilyTree {
    private ArrayList familyGroupList = new ArrayList();

    private String familyName;
    private String familyTask;

    FamilyGroup(String newFamilyName, String newFamilyTask){
        familyName= newFamilyName;
        familyTask=newFamilyTask;

    }
    private String getFamilyName() {
        return familyName;}
    private String getFamilyTask() {
        return familyTask;
    }

    public void add(FamilyTree newFamilyGroup){
        familyGroupList.add(newFamilyGroup);
    }

    public void displayInfo() {
        System.out.println(getFamilyName()+" " +
                getFamilyTask()+ "\n");

        for (Object family : familyGroupList) {
            FamilyTree familyInfo = (FamilyTree) family;
            familyInfo.displayInfo();
        }
    }
}


