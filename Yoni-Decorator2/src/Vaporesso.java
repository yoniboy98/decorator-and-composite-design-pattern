public class Vaporesso extends VapeDecorator {


    public Vaporesso(Vape newVape) {
        super(newVape);

    }

    @Override
    public double cost() {
        return vape.cost()+ 29.90;
    }

    @Override
    public String extraAttachment() {
        return vape.extraAttachment()+ "you added an extra vaporesso! ";
    }

    @Override
    public String extraFlavor() {
        return vape.extraFlavor() + "";
    }


}
