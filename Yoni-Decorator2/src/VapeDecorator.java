abstract class VapeDecorator implements Vape {
    protected Vape vape;

    public VapeDecorator(Vape newVape) {
        vape = newVape;
    }

    public String extraAttachment() {

        return vape.extraAttachment();
    }

    public double cost() {
        return vape.cost();
    }

    public String extraFlavor() {
        return vape.extraFlavor();
    }
}