public class VapeShopMain {
    public static void main(String[] args) {
        Vape vape= new Vaporesso(new ExtraBattery( new raspberryFlavor(new StrawberryFlavor(new BasicVape()))));
        System.out.println(vape.extraAttachment());
        System.out.println(vape.extraFlavor());

        System.out.println("the total cost of your order is: " + vape.cost() + "$");
    }
}
