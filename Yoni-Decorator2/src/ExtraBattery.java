public class ExtraBattery extends VapeDecorator{


    public ExtraBattery(Vape newVape) {
        super(newVape);

    }

    public double cost() {
        return vape.cost()+ 18.85;
    }

    @Override
    public String extraAttachment() {
        return vape.extraAttachment()+ "you added an extra battery! \n";
    }

    @Override
    public String extraFlavor() {
        return vape.extraFlavor() + "";
    }
}
