public class BasicVape implements Vape {

    @Override
    public double cost() {
        return 50.00;
    }

    @Override
    public String extraAttachment() {
        return  "";
    }

    @Override
    public String extraFlavor() {
        return  "";
    }
}
