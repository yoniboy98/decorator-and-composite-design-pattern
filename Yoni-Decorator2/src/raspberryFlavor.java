public class raspberryFlavor extends VapeDecorator {


    public raspberryFlavor(Vape newVape) {
        super(newVape);

    }

    @Override
    public double cost() {
        return vape.cost()+ 2.00;
    }

    @Override
    public String extraAttachment() {
        return vape.extraAttachment()+ "";
    }

    @Override
    public String extraFlavor() {
        return vape.extraFlavor()+ "you added raspberry flavor!";
    }
}
